var mongoose = require('mongoose');

var allCardsSchema = mongoose.Schema({
        nombre: String,
        color: String,
        colorOn: String,
        inicio: Number,
        fin: Number,
        grupo: String
});

module.exports = mongoose.model('AllCards', allCardsSchema);