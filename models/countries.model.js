var mongoose = require('mongoose');

var countriesSchema = mongoose.Schema({
        name: String,
        code: String
});

module.exports = mongoose.model('Country', countriesSchema);