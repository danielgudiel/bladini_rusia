
var cfg = require('../config/cfg.js');

var express     	= require('express'),
	bodyParser		= require('body-parser'),
	routes			= require('./routes/routes.js'),
	mongoose        = require('mongoose'),
	helmet			= require('helmet')

mongoose.Promise = global.Promise;

mongoose.connect(cfg.urlMongo);

mongoose.connection.on('error', function() {
    console.log('Could not connect to the database. Exiting now...');
    process.exit();
});

mongoose.connection.once('open', function() {
    console.log("Successfully connected to the database");
})

var app = express()

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.use(helmet())

routes(app)

app.listen(cfg.puerto, function() {
	console.log(cfg.messageTerminal );
});