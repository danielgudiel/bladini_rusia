
var card = require('../controllers/cards.controller.js')

var appRouter = function (app) {

  app.get("/", function(req, res) {
    res.status(200).send("API-BLADINI");
  });
/*
GET /cards
GET /regions
POST /user
POST /post
GET /post/region/{idRegion}
PUT /post/{idPost}
*/

  app.post('/api/cards', card.create);
  app.get('/api/cards', card.getAlbum);
  
  /*
  app.use('/api/auth', authController);
  
  app.post('/api/products', VerifyToken, products.create);
  app.get('/api/products/:idStore', VerifyToken, products.findAll);
  //app.get('/api/products/:productId', VerifyToken, products.findOne);
  app.put('/api/products/:idProduct', VerifyToken, products.update);
  //app.delete('/api/products/:noteId', VerifyToken, products.delete); // no se tiene pensado eliminar si no solamente cambiar un estado

  app.post('/api/client', VerifyToken, user.create)

  app.post('/api/store', VerifyToken, store.asociate)
  app.get('/api/store', VerifyToken, store.find)
  app.put('/api/store/:id', VerifyToken, store.update);
  */
}

module.exports = appRouter;