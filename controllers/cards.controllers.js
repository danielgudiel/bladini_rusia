
var AllCards = require('../models/allCards.model.js');

var msg = {
        codStatus   :0,
        status      :'',
        message     :'',
        result      :[]
    }

function setNullMSG(){
    msg.codStatus = 0
    msg.status = ''
    msg.message = ''
    msg.result = []
}

exports.create = function(req, res, next) {
    setNullMSG()
	var objectData = req.body.teams

    if(!objectData) {
        msg.codStatus   = 400
        msg.status      = 'fail' 
        msg.message     = 'No content send.'
        res.status(400).send(msg);
    }
    
    var tam = objectData.length
    var errorCycle = false
    var teamsSuccess = 0
    for(var i=0; i<tam; i++){

        var team = new AllCards(objectData[i])
        team.save(function(err, dataRes) {
            if(err) {
                errorCycle = true
                console.log(err);
            } else {
                teamsSuccess += 1
            }
        });
    }
    if(errorCycle){
        msg.codStatus   = 404
        msg.status      = 'fail' 
        msg.message     = 'Error to save teams.'
        msg.result      = {teamsSaved: teamsSuccess}
        res.status(404).send(msg);
    }else{
        msg.codStatus   = 200
        msg.status      = 'success' 
        msg.message     = 'teams(s) saved.'
        msg.result      = {teamsSaved: teamsSuccess}
        res.status(200).send(msg);
    }
};

exports.create Regions = function(req, res, next) {
    setNullMSG()
    var objectData = req.body.teams

    if(!objectData) {
        msg.codStatus   = 400
        msg.status      = 'fail' 
        msg.message     = 'No content send.'
        res.status(400).send(msg);
    }
    
    var tam = objectData.length
    var errorCycle = false
    var teamsSuccess = 0 
    for(var i=0; i<tam; i++){

        var team = new AllCards(objectData[i])
        team.save(function(err, dataRes) {
            if(err) {
                errorCycle = true
                console.log(err);
            } else {
                teamsSuccess += 1
            }
        });
    }
    if(errorCycle){
        msg.codStatus   = 404
        msg.status      = 'fail' 
        msg.message     = 'Error to save teams.'
        msg.result      = {teamsSaved: teamsSuccess}
        res.status(404).send(msg);
    }else{
        msg.codStatus   = 200
        msg.status      = 'success' 
        msg.message     = 'teams(s) saved.'
        msg.result      = {teamsSaved: teamsSuccess}
        res.status(200).send(msg);
    }
};

exports.getAlbum = function(req, res, next) {
    setNullMSG()
    
    AllCards.find({},function(err, teams){
        if(err){
            console.log(err)
            msg.codStatus   = 500
            msg.status      = 'fail' 
            msg.message     = 'Error occurred while retrieving teams.'
            res.status(500).send(msg);
        }else{
            if(products.length == 0){
                msg.codStatus   = 404
                msg.status      = 'fail' 
                msg.message     = 'No teams registered.'
                res.status(404).send(msg);
            }else{
                msg.codStatus   = 200
                msg.status      = 'success'
                msg.message     = 'Products found.'
                msg.result      = teams
                res.status(200).send(msg);
            }
        }
    })
};

exports.findOne = function(req, res, next) {

    Product.findById(req.params.productId, function(err, product) {
        if(err) {
            console.log(err);
            msg.codStatus   = 500
            msg.status      = 'fail' 
            msg.message     = 'Error retrieving product'
            return res.status(500).send(msg);
        } 

        if(!product) {
            msg.codStatus   = 404
            msg.status      = 'fail' 
            msg.message     = 'Product not found'
            return res.status(404).send(msg);
        }else{
            msg.codStatus   = 200
            msg.status      = 'success' 
            msg.message     = 'Product found'
            msg.result      = product
            res.status(200).send(msg);
        }
        
    });

};

exports.update = function(req, res, next) {
    
    setNullMSG()
    keyuuid.findOne({type:'pt',uuidGenerateKey:req.params.idProduct}, function(err, uuid){
        if(err) {
            console.log(err)
            msg.codStatus   = 403
            msg.status      = 'fail' 
            msg.message     = 'Wrong product.'
            res.status(403).send(msg);
        } 
        if(!uuid) {
            console.log(err)
            msg.codStatus   = 403
            msg.status      = 'fail' 
            msg.message     = 'Mistake product.'
            res.status(403).send(msg);
        }else{
            Product.findById(uuid.idBase, function(err, product) {

                var objectData = req.body
                if(err) {
                    console.log(err);
                    if(err.kind === 'ObjectId') {
                        msg.codStatus   = 404
                        msg.status      = 'fail' 
                        msg.message     = 'Product not found.'
                        return res.status(404).send(msg);
                    }
                    msg.codStatus   = 500
                    msg.status      = 'fail' 
                    msg.message     = 'Error finding product.'
                    return res.status(500).send(msg);
                }

                if(!product) {
                    msg.codStatus   = 404
                    msg.status      = 'fail' 
                    msg.message     = 'Product not found'
                    return res.status(404).send(msg);
                }

                product.category      = objectData.category ? objectData.category : product.category,
                product.brand         = objectData.brand ? objectData.brand:product.brand,
                product.size          = objectData.size ? objectData.size:product.size,
                product.color         = objectData.color ? objectData.color:product.color,
                product.material      = objectData.material ? objectData.material:product.material,
                product.name          = objectData.name ? objectData.name:product.name,
                product.price         = objectData.price ? objectData.price:product.price,
                product.description   = objectData.description ? objectData.description:product.description,
                product.available     = objectData.available ? objectData.available:product.available,
                product.discount      = objectData.discount ? objectData.discount:product.discount,
                product.tags          = objectData.tags ? objectData.tags:product.tags,
                product.deleted       = objectData.deleted ? objectData.deleted:product.deleted

                product.save(function(err, data){
                    if(err) {
                        console.log(err)
                        msg.codStatus   = 404
                        msg.status      = 'fail' 
                        msg.message     = 'Could not update product.'
                        res.status(404).send(msg);
                    } else {
                        msg.codStatus   = 200
                        msg.status      = 'success' 
                        msg.message     = 'Product updated.'
                        res.status(200).send(msg);
                    }
                });
            });
        }
    })
};
